from ast import Num, BinOp, UnaryOp
from lexer import INTEGER, PLUS, MINUS, MUL, DIV, LPAR, RPAR, EOF

class ParserException(Exception):
    pass

class Parser(object):
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = self.lexer.get_next_token()

    def error(self, msg):
        raise ParserException('Error parsing input: ' + msg)

    def eat(self, token_type):
        return self.eat_one_of([token_type])

    def eat_one_of(self, token_types):
        """Eat current token if it is one of token_types, otherwise raise an
        exception."""
        if self.current_token.type in token_types:
            self.current_token = self.lexer.get_next_token()
        else:
            self.error('Token of different type expected (expected '
                'one of types: {}, current type: {})'.format(
                        token_types, self.current_token.type))


    def factor(self):
        """
        factor: INTEGER | (PLUS|MINUS) factor | LPAR expr RPAR
        """
        token = self.current_token
        if token.type == INTEGER:
            self.eat(INTEGER)
            return Num(token)
        elif token.type == LPAR:
            self.eat(LPAR)
            node = self.expr()
            self.eat(RPAR)
            return node
        elif token.type == PLUS:
            self.eat(PLUS)
            node = self.factor()
            return UnaryOp(token, node)
        elif token.type == MINUS:
            self.eat(MINUS)
            node = self.factor()
            return UnaryOp(token, node)
        else:
            self.error("Syntax error")

    def term(self):
        """
        term    : factor((MUL|DIV) factor)*
        factor  : INTEGER
        """
        node = self.factor()

        while self.current_token.type in (MUL, DIV):
            token = self.current_token
            self.eat_one_of([MUL, DIV])
            node = BinOp(node, token, self.factor())
        return node

    def expr(self):
        """Return AST for expresion

        expr    : term((PLUS|MINUS) term)*
        term    : factor((MUL|DIV) factor)*
        factor  : INTEGER
        """
        node = self.term()

        while self.current_token.type in (PLUS, MINUS):
            token = self.current_token
            self.eat_one_of([PLUS, MINUS])
            node = BinOp(node, token, self.term())
        return node

    def parse(self):
        """Parse text got from tokenizer into ast"""
        ast = self.expr()
        if self.current_token.type != EOF:
            self.error("Syntax error")
        return ast
