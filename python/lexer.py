INTEGER = 'INTEGER'
PLUS, MINUS, MUL, DIV = 'PLUS', 'MINUS', 'MUL', 'DIV'
LPAR, RPAR = 'LPAR', 'RPAR'
EOF = 'EOF'

class LexerException(Exception):
    pass

class Token(object):
    def __init__(self, type, value):
        # token type: INTEGER, PLUS, or EOF
        self.type = type
        # token value: non-negative integer, '+', '-', '*', '/', or None
        self.value = value

    def __str__(self):
        """String representation of the class instance.

        Examples:
            Token(INTEGER, 3)
            Token(PLUS '+')
        """
        return 'Token({type}, {value})'.format(
            type=self.type,
            value=repr(self.value)
        )

    def __repr__(self):
        return self.__str__()


class Lexer(object):
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def error(self, msg):
        raise LexerException('Error parsing input: ' + msg)

    def skip_current_whitespace(self):
        while self.pos < len(self.text) and self.current_char.isspace():
            self.advance()

    def advance(self):
        """Advances pos by one, unless we are already at the end of text."""
        if self.pos < len(self.text):
            self.pos += 1
        if self.pos < len(self.text):
            self.current_char = self.text[self.pos]
        else:
            self.current_char = None

    def integer(self):
        """Parses current integer as integer."""
        str_integer = ''
        while self.current_char is not None and self.current_char.isdigit():
            str_integer += self.current_char
            self.advance()
        return int(str_integer)

    def get_next_token(self):
        """Lexical analyzer (also known as lexer, scanner or tokenizer)

        This method is responsible for breaking a sentence
        apart into tokens. One token at a time.
        """
        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_current_whitespace()
                continue

            if self.current_char.isdigit():
                return Token(INTEGER, self.integer())

            if self.current_char == '+':
                self.advance()
                return Token(PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(MINUS, '-')

            if self.current_char == '*':
                self.advance()
                return Token(MUL, '*')

            if self.current_char == '/':
                self.advance()
                return Token(DIV, '/')

            if self.current_char == '(':
                self.advance()
                return Token(LPAR, '(')

            if self.current_char == ')':
                self.advance()
                return Token(RPAR, ')')


            self.error('Invalid character "{}"'.format(self.current_char))

        return Token(EOF, None)
