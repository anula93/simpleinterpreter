from ast import BinOp, Num, NodeVisitor
from lexer import INTEGER, PLUS, MINUS, MUL, DIV, LPAR, RPAR

from lexer import Lexer
from parser import Parser

class RPNTranslatorException(Exception):
    pass

class RPNTranslator(NodeVisitor):
    def translate(self, text):
        lexer = Lexer(text)
        parser = Parser(lexer)
        ast = parser.parse()
        return self.visit(ast)

    def error(self, msg):
        raise RPNTranslatorException('Error in translator: ' + msg)

    def visit_Num(self, node):
        return str(node.value)

    def visit_UnaryOp(self, node):
        if node.op.type == PLUS:
            return "+" + self.visit(node.expr)
        elif node.op.type == MINUS:
            return "-" + self.visit(node.expr)

    def visit_BinOp(self, node):
        if node.op.type == PLUS:
            return self.visit(node.left) + " " + self.visit(node.right) + " +"
        elif node.op.type == MINUS:
            return self.visit(node.left) + " " + self.visit(node.right) + " -"
        elif node.op.type == MUL:
            return self.visit(node.left) + " " + self.visit(node.right) + " *"
        elif node.op.type == DIV:
            return self.visit(node.left) + " " + self.visit(node.right) + " /"
        self.error("Shouldn't have happened")


def main():
    while True:
        try:
            text = input('rpn_translator> ')
        except EOFError:
            break
        if not text:
            continue
        translator = RPNTranslator()
        result = translator.translate(text)
        print(result)


if __name__ == '__main__':
    main()
