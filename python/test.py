import unittest

from interpreter import Interpreter, InterpreterException
from lexer import Lexer, LexerException
from parser import Parser, ParserException

def interpret_text(text):
    lexer = Lexer(text)
    parser = Parser(lexer)
    interpreter = Interpreter(parser)
    return interpreter.interpret()


class TestInterpreter(unittest.TestCase):


    def test_addition(self):
        text = "2 + 2"
        val = interpret_text(text)
        self.assertEqual(val, 4)

    def test_substraction(self):
        text = "2 - 3"
        val = interpret_text(text)
        self.assertEqual(val, -1)

    def test_multidigit(self):
        text = "23 - 15+ 2"
        val = interpret_text(text)
        self.assertEqual(val, 10)

    def test_multi_div(self):
        tests = [
            ("22 / 2 * 3", 33),
            ("21 / 2 * 3", 30),
            ("3* 4 / 2", 6),
        ]
        for text, res in tests:
            val = interpret_text(text)
            self.assertEqual(val, res)

    def test_precendence(self):
        tests = [
            ("2 * 3 + 4", 10),
            ("4 + 3*2", 10),
            ("6 + 5/5 - 3*2", 1),
        ]
        for text, res in tests:
            val = interpret_text(text)
            self.assertEqual(val, res)

    def test_parenthises(self):
        tests = [
            ("2 * (3 + 4) ", 14),
            ("6 + 4/(5 - 3)*2", 10),
            ("7 + 3 * (10 / (12 / (3 + 1) - 1))", 22),
            (" 2 * ((3 + 3))", 12),
        ]
        for text, res in tests:
            val = interpret_text(text)
            self.assertEqual(val, res)

    def test_unary(self):
        tests = [
            ("-3", -3),
            ("--3", 3),
            ("- - - + - 3", 3),
            ("- 3 +4", 1),
            ("- 3 +-4", -7),
            ("5 - - - 2", 3),
        ]
        for text, res in tests:
            val = interpret_text(text)
            self.assertEqual(val, res)

    def test_wrong_syntax(self):
        text = "23 + 3 -"
        with self.assertRaises(ParserException):
            interpret_text(text)
        text = "1 (1+2)"
        with self.assertRaises(ParserException):
            interpret_text(text)

    def test_wrong_characters(self):
        text = "23 # 3"
        with self.assertRaises(LexerException):
            interpret_text(text)

if __name__ == '__main__':
    unittest.main()
