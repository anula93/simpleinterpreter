from ast import BinOp, Num, NodeVisitor
from lexer import INTEGER, PLUS, MINUS, MUL, DIV, LPAR, RPAR

class InterpreterException(Exception):
    pass

class Interpreter(NodeVisitor):
    def __init__(self, parser):
        self.parser = parser

    def error(self, msg):
        raise InterpreterException('Error parsing input: ' + msg)

    def visit_Num(self, node):
        return node.value

    def visit_UnaryOp(self, node):
        if node.op.type == PLUS:
            return self.visit(node.expr)
        elif node.op.type == MINUS:
            return -self.visit(node.expr)

    def visit_BinOp(self, node):
        if node.op.type == PLUS:
            return self.visit(node.left) + self.visit(node.right)
        elif node.op.type == MINUS:
            return self.visit(node.left) - self.visit(node.right)
        elif node.op.type == MUL:
            return self.visit(node.left) * self.visit(node.right)
        elif node.op.type == DIV:
            return self.visit(node.left) // self.visit(node.right)
        self.error("Shouldn't have happened")

    def interpret(self):
        return self.visit(self.parser.parse())
