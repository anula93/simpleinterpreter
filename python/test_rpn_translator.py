import unittest

from rpn_translator import RPNTranslator
from lexer import Lexer, LexerException
from parser import Parser, ParserException


class TestRPNTranslator(unittest.TestCase):
    def setUp(self):
        self.translator = RPNTranslator()

    def test_add_sub(self):
        tests = [
            ('2+2', '2 2 +'),
            ('4 - 2', '4 2 -'),
            ('2 + 4 - 2', '2 4 + 2 -'),
        ]
        for text, res in tests:
            val = self.translator.translate(text)
            self.assertEqual(val, res)

    def test_multi_div(self):
        tests = [
            ('22 / 2 * 3', '22 2 / 3 *'),
            ('21 / 2 * 3', '21 2 / 3 *'),
            ('3* 4 / 2', '3 4 * 2 /'),
        ]
        for text, res in tests:
            val = self.translator.translate(text)
            self.assertEqual(val, res)

    def test_precendence(self):
        tests = [
            ('2 * 3 + 4', '2 3 * 4 +'),
            ('4 + 3*2', '4 3 2 * +'),
            ('6 + 5/5 - 3*2', '6 5 5 / + 3 2 * -'),
        ]
        for text, res in tests:
            val = self.translator.translate(text)
            self.assertEqual(val, res)

    def test_parenthises(self):
        tests = [
            ('2 * (3 + 4) ', '2 3 4 + *'),
            ('6 + 4/(5 - 3)*2', '6 4 5 3 - / 2 * +'),
            ('7 + 3 * (10 / (12 / (3 + 1) - 1))', '7 3 10 12 3 1 + / 1 - / * +'),
            (' 2 * ((3 + 3))', '2 3 3 + *'),
        ]
        for text, res in tests:
            val = self.translator.translate(text)
            self.assertEqual(val, res)

    def test_unary(self):
        tests = [
            ('-3', '-3'),
            ('-+-3 + -4', '-+-3 -4 +'),
        ]
        for text, res in tests:
            val = self.translator.translate(text)
            self.assertEqual(val, res)

if __name__ == '__main__':
    unittest.main()
