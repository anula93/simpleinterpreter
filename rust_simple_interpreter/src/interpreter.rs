use std::error;
use std::error::Error;
use std::result::Result;
use std::fmt;
use std::convert;

use lexer;
use lexer::Token;
use parser;
use parser::{Parser, AST};

#[derive(Debug)]
pub struct InterpreterError {
    desc: String,
}

impl error::Error for InterpreterError {
    fn description(&self) -> &str {
        &self.desc
    }
}

impl fmt::Display for InterpreterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "InterpreterError: {}", self.desc)
    }
}

impl convert::From<lexer::LexerError> for InterpreterError {
    fn from(err: lexer::LexerError) -> InterpreterError {
        InterpreterError {
            desc: format!("From LexerError: '{}'",
                          err.description().to_string()),
        }
    }
}

impl convert::From<parser::ParserError> for InterpreterError {
    fn from(err: parser::ParserError) -> InterpreterError {
        InterpreterError {
            desc: format!("From ParserError: '{}'",
                          err.description().to_string()),
        }
    }
}

#[derive(Debug)]
pub struct Interpreter {
    parser: Parser,
}

impl Interpreter {
    pub fn new(pars: Parser) -> Interpreter {
        Interpreter {
            parser: pars,
        }
    }

    fn visit(ast: &Box<AST>) -> Result<i32, InterpreterError> {
        match **ast {
            AST::Num(x) => Ok(x),
            AST::UnaryOp{ref child, ref op} => {
                match *op {
                    Token::PLUS =>
                        Ok(try!(Interpreter::visit(child))),
                    Token::MINUS =>
                        Ok(-try!(Interpreter::visit(child))),
                    _ => unreachable!(),
                }
            },
            AST::BinOp{ref left, ref right, ref op} => {
                match *op {
                    Token::PLUS =>
                        Ok(try!(Interpreter::visit(left))
                           + try!(Interpreter::visit(right))),
                    Token::MINUS =>
                        Ok(try!(Interpreter::visit(left))
                           - try!(Interpreter::visit(right))),
                    Token::MUL =>
                        Ok(try!(Interpreter::visit(left))
                           * try!(Interpreter::visit(right))),
                    Token::DIV =>
                        Ok(try!(Interpreter::visit(left))
                           / try!(Interpreter::visit(right))),
                    _ => unreachable!(),
                }
            }
        }
    }

    /// expr: term ((PLUS|MINUS) term)*
    /// term: factor ((MUL|DIV) factor)*
    /// factor:  (PLUS|MINUS) factor | INTEGER | LPAREN expr RPAREN
    pub fn interpret(&mut self) -> Result<i32, InterpreterError> {
        let tree = Box::new(try!(self.parser.parse()));
        return Interpreter::visit(&tree);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lexer::*;
    use parser::*;

    fn create_interpreter(line: &str) -> Interpreter {
        let lexer = Lexer::new(line);
        let parser = Parser::new(lexer);
        Interpreter::new(parser)
    }

    fn validate_expression_samples(samples: &[(&str, i32)]) {
        for &(test, corr_res) in samples.iter() {
            println!("Validating '{}'...", test);
            let mut inter = create_interpreter(test);
            let res = match inter.interpret() {
                Ok(x) => x,
                Err(e) => panic!("Err({}) when Ok should be returned.", e),
            };
            assert_eq!(res, corr_res);
        }
    }

    #[test]
    fn simple_addition_subtraction() {
        let samples = vec![
            ("2+3", 5),
            ("9+0", 9),
            ("9+6", 15),
            ("2-3", -1),
            ("9-0", 9),
            ("9-6", 3),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn additional_whitespaces() {
        let samples = vec![
            ("2  +3", 5),
            ("9+ 0 ", 9),
            ("9 + 6", 15),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn multidigit() {
        let samples = vec![
            ("22  +3", 25),
            ("9+ 11 ", 20),
            ("19 + 6", 25),
            ("39+ 127", 166),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn complicated_addition_multiplication() {
        let samples = vec![
            ("3 / 2", 1),
            ("10 + 2 * 10 ", 30),
            ("10 + 2 * 10 / 2 - 5", 15),
            ("14 + 2 * 3 - 6 / 2", 17),
            ("7 - 8 / 4", 5),
            ("2 + 7 * 4", 30),
            ("24", 24),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn parens() {
        let samples = vec![
            ("3 / 2", 1),
            ("(10 + 2) * 10 ", 120),
            ("(10 + 2) * 10 / (2 - 5)", -40),
            ("7 + 3 * (10 / (12 / (3 + 1) - 1))", 22),
            ("14 + 2 * (3 - 6) / 2", 11),
            ("(7 - 8) / 4", 0),
            ("2 + (7 * 4)", 30),
            ("24", 24),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn unary() {
        let samples = vec![
            ("- 3", -3),
            ("+ 3", 3),
            ("+ - 3", -3),
            ("+ (- (3))", -3),
            ("5 -- 2", 7),
            ("5 - - -+ - 3", 8),
            ("5 - - - + - (3 + 4) - +2", 10),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn general() {
        let samples = vec![
            ("7 + 3 * (10 / (12 / (3 + 1) - 1))", 22),
            ("7 + 3 * (10 / (12 / (3 + 1) - 1)) / (2 + 3) - 5 - 3 + (8) ", 10),
            ("7 + (((3 + 2)))", 12),
        ];
        validate_expression_samples(&samples);
    }

    #[test]
    fn wrong_syntax() {
        let samples = vec![
            "(10 + 2) 10 ",
            "()",
            "2 4",
            "38 - (/)",
            "1 +",
            "1 (1+2)",
        ];
        for test in samples.iter() {
            println!("Validating '{}'...", test);
            let mut inter = create_interpreter(test);
            match inter.interpret() {
                Ok(x) => panic!("Ok({}) when Err should be returned.", x),
                _ => continue,
            };
        }
    }
}
