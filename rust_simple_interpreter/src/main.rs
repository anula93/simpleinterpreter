mod lexer;
mod parser;
mod interpreter;

use std::io;
use std::io::Write;

fn main() {
    let prompt = "$> ";
    loop {
        print!("{}", prompt);
        io::stdout().flush().unwrap();
        let mut line = String::new();
        match io::stdin().read_line(&mut line) {
            Ok(0) => {
                println!("\nBye!");
                break;
            },
            Err(e) => {
                println!("Error reading input: {}", e);
                continue;
            }
            Ok(_) => {},
        };
        let lexer = lexer::Lexer::new(&line);
        let parser = parser::Parser::new(lexer);
        let mut inter = interpreter::Interpreter::new(parser);
        match inter.interpret() {
            Ok(res) => println!("{}", res),
            Err(err) => println!("An error occured: {}", err),
        };
    }
}
