use std::boxed::Box;
use std::convert;
use std::error;
use std::error::Error;
use std::fmt;
use std::result::Result;

use lexer;
use lexer::{Lexer, Token};

#[derive(Debug)]
pub struct ParserError {
    desc: String,
}

impl error::Error for ParserError {
    fn description(&self) -> &str {
        &self.desc
    }
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ParserError: {}", self.desc)
    }
}

impl convert::From<lexer::LexerError> for ParserError {
    fn from(err: lexer::LexerError) -> ParserError {
        ParserError {
            desc: format!("From LexerError: '{}'",
                          err.description().to_string()),
        }
    }
}


#[derive(Debug)]
pub enum AST {
    Num(i32),
    BinOp {left: Box<AST>, right: Box<AST>, op: Token},
    UnaryOp {child: Box<AST>, op: Token},
}

#[derive(Debug)]
pub struct Parser {
    lexer: Lexer,
    current_token: Option<Token>,
}

impl Parser {
    pub fn new(lex: Lexer) -> Parser {
        Parser {
            lexer: lex,
            current_token: None,
        }
    }

    /// expr: term ((PLUS|MINUS) term)*
    /// term: factor ((MUL|DIV) factor)*
    /// factor:  (PLUS|MINUS) factor | INTEGER | LPAREN expr RPAREN
    pub fn parse(&mut self) -> Result<AST, ParserError> {
        try!(self.advance_current_token());
        let res = try!(self.expr());
        match self.current_token {
            Some(Token::EOF) => Ok(res),
            _ => Err(ParserError{desc: format!("Not whole line was parsed")})
        }
    }

    fn advance_current_token(&mut self) -> Result<(), ParserError> {
        self.current_token = Some(try!(self.lexer.get_next_token()));
        Ok(())
    }

    fn factor(&mut self) -> Result<AST, ParserError> {
        match self.current_token.clone() {
            Some(t @ Token::PLUS) | Some(t @ Token::MINUS) => {
                try!(self.advance_current_token());
                let child = try!(self.factor());
                let root = AST::UnaryOp {
                    child: Box::new(child),
                    op: t,
                };
                Ok(root)
            },
            Some(Token::INTEGER(x)) => {
                try!(self.advance_current_token());
                Ok(AST::Num(x))
            },
            Some(Token::LPAREN) => {
                try!(self.advance_current_token());
                let x = try!(self.expr());
                if let Some(Token::RPAREN) = self.current_token {
                    try!(self.advance_current_token());
                    Ok(x)
                } else {
                    Err(ParserError
                            {desc: format!("Expected right paren got {:?}",
                                           self.current_token )})
                }
            },
            Some(ref t) => Err(ParserError
                            {desc: format!("Expected integer got {:?}", t)}),
            _  => Err(ParserError
                            {desc: format!("Expected integer got None")})
        }
    }
    fn term(&mut self) -> Result<AST, ParserError> {
        let mut root = try!(self.factor());
        while match self.current_token {
            Some(Token::MUL) | Some(Token::DIV) => true,
            _ => false,
        } {
            let op = if let Some(op) = self.current_token.clone() {
                op
            } else {
                unreachable!();
            };
            try!(self.advance_current_token());
            let right = try!(self.factor());
            let left = root;
            root = AST::BinOp {
                left: Box::new(left),
                right: Box::new(right),
                op: op,
            };
        }
        Ok(root)
    }

    fn expr(&mut self) -> Result<AST, ParserError> {
        let mut root = try!(self.term());
        while match self.current_token {
            Some(Token::PLUS) | Some(Token::MINUS) => true,
            _ => false,
        } {
            let op = if let Some(op) = self.current_token.clone() {
                op
            } else {
                unreachable!();
            };
            try!(self.advance_current_token());
            let right = try!(self.term());
            let left = root;
            root = AST::BinOp {
                left: Box::new(left),
                right: Box::new(right),
                op: op,
            };
        }
        Ok(root)
    }

}
