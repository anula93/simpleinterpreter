use std::error;
use std::result::Result;
use std::fmt;

#[derive(Debug)]
pub struct LexerError {
    desc: String,
}

impl error::Error for LexerError {
    fn description(&self) -> &str {
        &self.desc
    }
}

impl fmt::Display for LexerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "LexerError: {}", self.desc)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Token {
    INTEGER(i32),
    PLUS,
    MINUS,
    MUL,
    DIV,
    LPAREN,
    RPAREN,
    EOF,
}

#[derive(Debug)]
pub struct Lexer {
    chars: Vec<char>,
    current_char: Option<char>,
    pos: usize,
}

impl Lexer {
    pub fn new(text: &str) -> Lexer {
        let chars: Vec<char> = text.chars().collect();
        let pos = 0;
        Lexer {
            current_char: Some(chars[pos]),
            pos: pos,
            chars: chars,
        }
    }

    pub fn get_next_token(&mut self) -> Result<Token, LexerError> {
        loop {
            match self.current_char {
                Some(c) if c.is_whitespace() => {
                    self.skip_whitespaces();
                    continue;
                },
                Some(c) if c.is_digit(10) => {
                    let num = self.integer();
                    return Ok(Token::INTEGER(num));
                },
                Some(c) => {
                    match c {
                        '+' => {
                            self.advance();
                            return Ok(Token::PLUS);
                        },
                        '-' => {
                            self.advance();
                            return Ok(Token::MINUS);
                        },
                        '*' => {
                            self.advance();
                            return Ok(Token::MUL);
                        },
                        '/' => {
                            self.advance();
                            return Ok(Token::DIV);
                        },
                        '(' => {
                            self.advance();
                            return Ok(Token::LPAREN);
                        },
                        ')' => {
                            self.advance();
                            return Ok(Token::RPAREN);
                        },
                        _ => return Err(LexerError {
                            desc: format!("Cannot parse char '{:?}' as token",
                                          self.current_char),
                        }),
                    }
                },
                None => return Ok(Token::EOF),
            }
        }
    }

    fn advance(&mut self) {
        if self.pos < self.chars.len() {
            self.pos += 1;
        }
        self.current_char = match self.pos {
            x if x < self.chars.len() => Some(self.chars[x]),
            _ => None,
        };
    }

    fn skip_whitespaces(&mut self) {
        while let Some(curr_char) = self.current_char {
            if !curr_char.is_whitespace() {
                return;
            }
            self.advance();
        }
    }

    /// Assumes that at least current_char is a digit
    fn integer(&mut self) -> i32 {
        let mut number: i32 = 0;
        let mut is_number: bool = false;
        while let Some(curr_char) = self.current_char {
            if !curr_char.is_digit(10)  {
                break;
            }
            is_number = true;
            number *= 10;
            number += curr_char.to_digit(10)
                .expect("c.is_digit(10) failed c.to_digit(10)") as i32;
            self.advance();
        }
        if !is_number {
            panic!(format!(
                    "Lexer.integer called on non digit char: {:?}",
                    self.current_char
            ));
        }
        number
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    fn validate_tokens_returned<T>(samples: &[(&str, T)])
                where T: AsRef<[Token]> {
        for i in 0..samples.len() {
            let test: &str = samples[i].0;
            let corr_tokens: &[Token] = samples[i].1.as_ref();
            println!("Checking for: {}", test);
            let mut lexer = Lexer::new(test);
            for corr_token in corr_tokens.iter() {
                match lexer.get_next_token() {
                    Ok(ref x) => assert_eq!(x, corr_token),
                    Err(e) => panic!("Err({}) when Ok should be returned.", e),
                }
            }
        }
    }

    #[test]
    fn addition_substraction() {
        let samples: Vec<(&str, Vec<Token>)> = vec![
            ("45 + 2 ", vec![Token::INTEGER(45), Token::PLUS, Token::INTEGER(2)]),
            ("4 -25 ", vec![Token::INTEGER(4), Token::MINUS, Token::INTEGER(25)]),
            ("4-25+453 12", vec![
                Token::INTEGER(4), Token::MINUS, Token::INTEGER(25),
                Token::PLUS, Token::INTEGER(453), Token::INTEGER(12),
            ]),
            ("(4-25+)453* 12-/-", vec![
                Token::LPAREN,
                Token::INTEGER(4),
                Token::MINUS,
                Token::INTEGER(25),
                Token::PLUS,
                Token::RPAREN,
                Token::INTEGER(453),
                Token::MUL,
                Token::INTEGER(12),
                Token::MINUS,
                Token::DIV,
                Token::MINUS,
            ]),
        ];
        validate_tokens_returned(&samples);
    }
}
