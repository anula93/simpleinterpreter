# Simple interpreter #

Built according to [Let’s Build A Simple Interpreter](https://ruslanspivak.com/lsbasi-part1/).

Has Python3 and Rust versions.


### Python version ###

To run the code use following commands:
```
#!bash
cd python/
python3 calc.py
```
To run the tests:
```
#!bash
cd python/
python3 test.py
```

### Rust version ###
To run the code use following commands:
```
#!bash
cd rust_simple_interpreter/
cargo run
```
To run the tests:
```
#!bash
cd rust_simple_interpreter/
cargo test
```